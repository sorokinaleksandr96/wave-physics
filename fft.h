#ifndef FFT_H
#define FFT_H

#include <complex>
#include <QVector>
typedef std::complex<double> base;

class fft
{
    QVector<base> res;

    int  getNext2Pow(int N);
    void fill2Pow();
    void fftAlgorithm();
public:
    fft(const QVector<double> &value);

    void process();
    QVector<double> getRealValue(double dnu);
    QVector<double> getArg(double dt);
};

#endif // FFT_H
