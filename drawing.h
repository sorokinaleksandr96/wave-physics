#ifndef DRAWING_H
#define DRAWING_H

#include "submode.h"

class Drawing: public QObject, public Submode
{
    Q_OBJECT
private:
    QPushButton *clear;
    bool is_drawing, is_drawed, is_splined;

    QVector<double> a,b,c,d,f; //spline coefficients and frequency points
    void splineProcess();
    double getSplineValue(double x);
public:
    Drawing(SUBMODE_TYPE newtype, QCustomPlot *newspectrum, QCustomPlot *newsignal, QPushButton *newclear, QLabel *newsubheader);

    void replot();
    void changeParametr(size_t index, double value);
    ~Drawing();
private slots:
    void startDrawing(QMouseEvent *event);
    void moveDrawing(QMouseEvent *event);
    void endDrawing();
};

#endif // DRAWING_H
