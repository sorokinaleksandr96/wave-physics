#ifndef CONTINUOUS_H
#define CONTINUOUS_H

#include "submode.h"

class Continuous: public Submode
{
    QDoubleSpinBox *center, *width;
    QLabel *widthlabel;
public:
    Continuous(SUBMODE_TYPE newtype, QDoubleSpinBox *newcenter, QDoubleSpinBox *newwidth, QCustomPlot *newspectrum, QCustomPlot *newsignal, QLabel *newsubheader, QLabel *newwidthlabel);
    void replot();
    void plot_theory();
    void changeParametr(size_t index, double value);

    double getValue(double mu, double width, double x);

    ~Continuous();

};

#endif // CONTINUOUS_H
