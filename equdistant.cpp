#include "equdistant.h"

Equdistant::Equdistant(SUBMODE_TYPE newtype, QSpinBox *newnumber, QDoubleSpinBox *newcenter, QDoubleSpinBox *newstep, QCustomPlot *newspectrum, QCustomPlot *newsignal, QLabel *newsubheader) :
    QObject(0), Submode(newtype, newspectrum, newsignal, newsubheader), number(newnumber), center(newcenter), step(newstep),
    moving(false), movingIndex(-1)
{
    changeParametr(1, 4.0);
    changeParametr(2, 0.01);
    changeParametr(0, 40);
    needToReplot = true;
    replot();

    subheader->setText(QString("Волновой пакет"));

    //mouse interface
    connect(spectrum, SIGNAL(mousePress(QMouseEvent*)), this, SLOT(pressSpectrum(QMouseEvent*)));
    connect(spectrum, SIGNAL(mouseMove(QMouseEvent*)), this, SLOT(moveSpectrum(QMouseEvent*)));
    connect(spectrum, SIGNAL(mouseRelease(QMouseEvent*)), this, SLOT(releaseSpectrum()));
}

bool Equdistant::check(int checkingSize, double checkingCenter, double checkingStep)
{
    double offset = (checkingSize / 2) * checkingStep + ((checkingSize + 1) % 2) * checkingStep / 2;
    if (checkingCenter + offset > 5.0 || checkingCenter - offset < 0.0) {
        return false;
    } else {
        return true;
    }
}

void Equdistant::changeParametr(size_t index, double value)
{
    switch (index)
    {
        case 0:
            if (check(int(value), center->value(), step->value())) {
                number->setValue(int(value));
            } else  {
                number->setValue(specX.size());
            }
        break;
        case 1:
            if (check(number->value(), value, step->value())) {
                center->setValue(value);
                centerWidth = value;
            } else {
                center->setValue(centerWidth);
            }
        break;
        case 2:
            if (check(number->value(), center->value(), value)) {
                step->setValue(value);
                widthStep = value;
            } else {
                step->setValue(widthStep);
            }
        break;
    }

    replot();
}

void Equdistant::replot()
{
    if (needToReplot == false)
        return;

    //spectrum replot
    specX.resize(number->value());
    specY.resize(number->value());

    for (int i = 0; i < specX.size(); i++) {
        specX[i] = center->value() + (i - specX.size() / 2) * step->value() + step->value() / 2 * ((specX.size() + 1) % 2);
        specY[i] = 1;
    }

    spectrum->clearGraphs();
    spectrum->addGraph();
    spectrum->graph()->setData(specX, specY);
    spectrum->graph()->setLineStyle(QCPGraph::lsImpulse);
    spectrum->graph()->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle, 10));
    spectrum->graph()->setPen(specPen);
    spectrum->replot();


    //signal replot
    //double T = 1.0 / widthStep;
    double T = 10;
    unsigned int signalSize = 75 * round(2 * T) + 1;
    //unsigned int signalSize = 4001;
    sigX.resize(signalSize); sigY.resize(signalSize);
    double dt = 2 * T / (sigX.size() - 1);
    for (int i = 0; i < sigX.size(); i++) {
        sigY[i] = 0.0;
        sigX[i] = i * dt - T;
        for(int j = 0; j < specX.size(); j++) {
            sigY[i] += specY[j] * cos(2 * M_PI * specX[j] * sigX[i]);
        }
        sigY[i] /= specX.size();
    }

    signal->clearGraphs();
    signal->addGraph();
    signal->graph()->setData(sigX, sigY);
    signal->graph()->setPen(sigPen);
    //signal->xAxis->setRange(-T, T);
    signal->replot();
}

void Equdistant::pressSpectrum(QMouseEvent *event)
{
    double x;
    x = spectrum->xAxis->pixelToCoord(event->pos().x());

    double eps = 0.03;
    for (int i = 0; i < specX.size(); i++) {
        if (fabs(specX[i] - x) < eps) {
            moving = true;
            movingIndex = i;
            break;
        }
    }
}

void Equdistant::moveSpectrum(QMouseEvent *event)
{
    if (moving == true) {
        double dx;
        dx = spectrum->xAxis->pixelToCoord(event->pos().x()) - specX[movingIndex];
        dx = round(dx * 10) / 10;

        changeParametr(1, center->value() + dx);
    }
}

void Equdistant::releaseSpectrum()
{
    moving = false;
    movingIndex = -1;
}

Equdistant::~Equdistant()
{
    //disable mouse interface
    disconnect(spectrum, SIGNAL(mousePress(QMouseEvent*)), this, SLOT(pressSpectrum(QMouseEvent*)));
    disconnect(spectrum, SIGNAL(mouseMove(QMouseEvent*)), this, SLOT(moveSpectrum(QMouseEvent*)));
    disconnect(spectrum, SIGNAL(mouseRelease(QMouseEvent*)), this, SLOT(releaseSpectrum()));

    signal->xAxis->setRange(-10, 10);
}
