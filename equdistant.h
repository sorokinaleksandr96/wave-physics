#ifndef EQUDISTANT_H
#define EQUDISTANT_H

#include "submode.h"

class Equdistant: public QObject, public Submode
{
    Q_OBJECT
private:
    QSpinBox *number;
    QDoubleSpinBox *center, *step;
    bool moving;
    int movingIndex;
    double centerWidth, widthStep;

    bool check(int checkingSize, double checkingCenter, double checkingStep);
public:
    Equdistant(SUBMODE_TYPE newtype, QSpinBox *newnumber, QDoubleSpinBox *newcenter, QDoubleSpinBox *newstep, QCustomPlot *newspectrum, QCustomPlot *newsignal, QLabel *newsubheader);
    void replot();
    void changeParametr(size_t index, double value);
    ~Equdistant();
private slots:
    void pressSpectrum(QMouseEvent *event);
    void moveSpectrum(QMouseEvent *event);
    void releaseSpectrum();
};

#endif // EQUDISTANT_H
