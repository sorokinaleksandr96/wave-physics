#ifndef MODE_H
#define MODE_H

#include <QComboBox>
#include <QLabel>

#include "submode.h"
#include "nonequdistant.h"
#include "equdistant.h"
#include "continuous.h"
#include "drawing.h"

#include "ui_mainwindow.h"

enum MODE_TYPE
{
    DISCRETE_MODE,
    CONTINUOUS_MODE
};


class Mode
{
    MODE_TYPE type;
    QComboBox *changeSub;
    QLabel *header;
    Ui::MainWindow *ui;
public:
    Submode *smode;
    Mode(MODE_TYPE newtype, Ui::MainWindow *newui);
    ~Mode();
    void changeSubmode(int index);
private slots:
};

#endif // MODE_H
