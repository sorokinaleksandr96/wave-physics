#ifndef NONEQUDISTANT_H
#define NONEQUDISTANT_H

#include "submode.h"
#include "QObject"

class Nonequdistant: public QObject, public Submode
{
    Q_OBJECT
private:
    QSpinBox *number, *current;
    QDoubleSpinBox *freq, *ampl;
    bool moving;

    void setView(int i);
public:
    Nonequdistant(SUBMODE_TYPE newtype, QSpinBox *newnumber, QSpinBox *newcurrent, QDoubleSpinBox *newfreq, QDoubleSpinBox *newampl, QCustomPlot *newspectrum, QCustomPlot *newsignal, QLabel *newsubheader);
    void replot();
    void changeParametr(size_t index, double value); // 0 - количество, 1 - текущая, 2 - частота, 3 - амплитуда
    ~Nonequdistant();

private slots:
    void pressSpectrum(QMouseEvent *event);
    void moveSpectrum(QMouseEvent *event);
    void releaseSpectrum();
};

#endif // NONEQUDISTANT_H
