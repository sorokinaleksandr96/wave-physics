#include "mode.h"

Mode::Mode(MODE_TYPE newtype, Ui::MainWindow *newui) : type(newtype), changeSub(newui->comboBox), header(newui->label_7), ui(newui), smode(0)
{
    ui->stackedWidget->setCurrentIndex(1);

    switch (newtype)
    {
        case DISCRETE_MODE:
            header->setText(QString("Дискретный спектр"));
            changeSub->addItem(QString("Отдельные гармоники"));
            changeSub->addItem(QString("Волновой пакет"));
        break;
        case CONTINUOUS_MODE:
            header->setText(QString("Непрерывный спектр"));
            changeSub->addItem(QString("Гауссов"));
            changeSub->addItem(QString("Прямоугольный"));
            changeSub->addItem(QString("Лоренцовский"));
            changeSub->addItem(QString("Произвольный"));
        break;
    }

    changeSubmode(0);
}

Mode::~Mode()
{
    ui->stackedWidget->setCurrentIndex(0);

    while(changeSub->count() > 0)
    {
        changeSub->removeItem(0);
    }

    if (smode != 0)
    {
        delete smode;
        smode = 0;
    }
}

void Mode::changeSubmode(int index)
{
    if (smode != 0)
    {
        delete smode;
        smode = 0;
    }

    if (type == DISCRETE_MODE)
    {
        switch (index)
        {
            case 0:
                smode = new Nonequdistant(NONEQUDISTANT_SUBMODE, ui->spinBox_2, ui->spinBox_3, ui->doubleSpinBox_3, ui->doubleSpinBox_4, ui->widget, ui->widget_2, ui->label_9);
                ui->stackedWidget_2->setCurrentIndex(0);
            break;

            case 1:
                smode = new Equdistant(EQUDISTANT_SUBMODE, ui->spinBox, ui->doubleSpinBox_9, ui->doubleSpinBox_10, ui->widget, ui->widget_2, ui->label_9);
                ui->stackedWidget_2->setCurrentIndex(1);
            break;
        }
    }
    else if (type == CONTINUOUS_MODE)
    {
        switch (index)
        {
            case 0:
                smode = new Continuous(GAUSSIAN_SUBMODE, ui->doubleSpinBox, ui->doubleSpinBox_2, ui->widget, ui->widget_2, ui->label_9, ui->label_2);
                ui->stackedWidget_2->setCurrentIndex(2);
            break;

            case 1:
                smode = new Continuous(RECTANGLE_SUBMODE, ui->doubleSpinBox, ui->doubleSpinBox_2, ui->widget, ui->widget_2, ui->label_9, ui->label_2);
                ui->stackedWidget_2->setCurrentIndex(2);
            break;

            case 2:
                smode = new Continuous(LORENZ_SUBMODE, ui->doubleSpinBox, ui->doubleSpinBox_2, ui->widget, ui->widget_2, ui->label_9, ui->label_2);
                ui->stackedWidget_2->setCurrentIndex(2);
            break;

            case 3:
                smode = new Drawing(SELFDRAWING_SUBMODE, ui->widget, ui->widget_2, ui->pushButton_5, ui->label_9);
                ui->stackedWidget_2->setCurrentIndex(3);
            break;
        }
    }
}
