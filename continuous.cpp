#include "continuous.h"
#include "fft.h"

Continuous::Continuous(SUBMODE_TYPE newtype, QDoubleSpinBox *newcenter, QDoubleSpinBox *newwidth, QCustomPlot *newspectrum, QCustomPlot *newsignal, QLabel *newsubheader, QLabel *newwidthlabel):
    Submode(newtype, newspectrum, newsignal, newsubheader), center(newcenter), width(newwidth), widthlabel(newwidthlabel)
{
    changeParametr(0, 3);
    needToReplot = true;
    if (newtype == GAUSSIAN_SUBMODE)
    {
        widthlabel->setText("Ширина по уровню e<sup>-1</sup>");
        subheader->setText(QString("Гауссов спектр"));
        changeParametr(1, 0.6);
    }
    else if (newtype == RECTANGLE_SUBMODE)
    {
        widthlabel->setText("Ширина");
        subheader->setText(QString("Прямоугольный спектр"));
        changeParametr(1, 0.6);
    }
    else if (newtype == LORENZ_SUBMODE)
    {
        widthlabel->setText("Ширина");
        subheader->setText(QString("Лоренцовский спектр"));
        changeParametr(1, 0.2);
    }
    replot();
}

void Continuous::changeParametr(size_t index, double value)
{
    switch(index)
    {
        case 0:
            center->setValue(value);
        break;
        case 1:
            width->setValue(value);
        break;
    }

    replot();
}

double Continuous::getValue(double mu, double width, double x)
{
    switch(type)
    {
        case GAUSSIAN_SUBMODE:
            return exp(- (x - mu) * (x - mu) / (2 * width * width));
        case RECTANGLE_SUBMODE:
            return (x <= mu + width / 2) && (x >= mu - width / 2) ? 1 : 0;
        case LORENZ_SUBMODE:
            return 1.0 / (1 + (x - mu) * (x - mu) / width / width);
        default:
            return 0;
    }
}

void Continuous::plot_theory()
{
    QVector<double> s(1001), t(1001);
    double dt = 20.0 / (s.size() - 1);
    for (int i = 0; i < s.size(); i++) {
        t[i] = -10.0 + i * dt;
        if (type == GAUSSIAN_SUBMODE) {
            //normed theoretical result
            double widthGauss = width->value() / (2 * sqrt(2));
            s[i] = exp(-widthGauss * widthGauss * t[i] * t[i] * 2 * M_PI * M_PI);
            s[i] *=  cos(2 * M_PI * center->value() * t[i]); //св-во смещения
        }
        if (type == RECTANGLE_SUBMODE) {
            //normed theoretical result
            if (i == s.size() / 2) {
                s[i] = 1.0;
            } else {
                s[i] = sin( M_PI * width->value() * t[i]) / (M_PI * width->value() * t[i]);
            }
            s[i] *=  cos(2 * M_PI * center->value() * t[i]); //св-во смещения
        }
        if (type == LORENZ_SUBMODE) {
            if (t[i] >= 0) {
                s[i] = exp(-2 * M_PI * width->value() * fabs(t[i]));
                s[i] *= cos(2 * M_PI * center->value() * t[i]);
            } else {
                s[i] = 0;
            }
        }
    }

    signal->addGraph();
    signal->graph()->setData(t, s);
    signal->graph()->setPen(sigPen);
}

void Continuous::replot()
{
    if (needToReplot == false)
        return;

    specX.resize(501);
    specY.resize(501);

    for(int i = 0; i < specX.size(); i++)
    {
        specX[i] = i * 0.01;
        if (type == GAUSSIAN_SUBMODE) {
            double widthGauss = width->value() / (2 * sqrt(2));
            specY[i] = getValue(center->value(), widthGauss, specX[i]);
        } else {
            specY[i] = getValue(center->value(), width->value(), specX[i]);
        }
    }


    spectrum->clearGraphs();
    spectrum->addGraph();
    spectrum->graph()->setData(specX, specY);
    spectrum->graph()->setPen(specPen);
    if (type == RECTANGLE_SUBMODE) {
        spectrum->graph()->setLineStyle(QCPGraph::lsStepRight);
    }
    if (type == GAUSSIAN_SUBMODE) {
        spectrum->addGraph();
        spectrum->graph()->setData({0, 1, 2, 3, 4, 5}, {1.0 / M_E, 1.0 / M_E, 1.0 / M_E, 1.0 / M_E, 1.0 / M_E, 1.0 / M_E});
        QPen LinePen(Qt::red, 2);
        LinePen.setStyle(Qt::DashLine);
        LinePen.setColor(QColor(255, 0, 0, 158));
        spectrum->graph()->setPen(LinePen);
    }
    spectrum->replot();


    signal->clearGraphs();
    plot_theory();
    signal->replot();
}

Continuous::~Continuous()
{

}
