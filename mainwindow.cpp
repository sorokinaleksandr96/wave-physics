#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "QDebug"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow), mode(0)
{
    ui->setupUi(this);

    // Ширина в непрерывном режиме
    ui->doubleSpinBox_2->setDecimals(2);
    ui->doubleSpinBox_2->setMinimum(0.01);
    ui->doubleSpinBox_2->setMaximum(3);
    ui->doubleSpinBox_2->setSingleStep(0.01);

    ui->doubleSpinBox->setMinimum(0);
    ui->doubleSpinBox->setMaximum(5);
    ui->doubleSpinBox->setDecimals(1);
    ui->doubleSpinBox->setSingleStep(0.1);


    // Неэквидистантный режим
    ui->spinBox_2->setMinimum(1);
    ui->spinBox_2->setMaximum(3);
    ui->spinBox_3->setMinimum(1);

    ui->doubleSpinBox_3->setMinimum(0);
    ui->doubleSpinBox_3->setMaximum(5);
    ui->doubleSpinBox_3->setDecimals(1);
    ui->doubleSpinBox_3->setSingleStep(0.1);

    ui->doubleSpinBox_4->setMinimum(0);
    ui->doubleSpinBox_4->setMaximum(1);
    ui->doubleSpinBox_4->setDecimals(1);
    ui->doubleSpinBox_4->setSingleStep(0.1);




    // Эквидистантный режим
    ui->spinBox->setMaximum(50);
    ui->spinBox->setMinimum(1);

    ui->doubleSpinBox_10->setDecimals(2);
    ui->doubleSpinBox_10->setMinimum(0.01);
    ui->doubleSpinBox_10->setMaximum(0.1);
    ui->doubleSpinBox_10->setSingleStep(0.01);

    ui->doubleSpinBox_9->setMinimum(0);
    ui->doubleSpinBox_9->setMaximum(5);
    ui->doubleSpinBox_9->setSingleStep(0.1);
    ui->doubleSpinBox_9->setDecimals(1);




    ui->widget->xAxis->setRange(0, 5);
    ui->widget->xAxis->setAutoTickStep(false);
    ui->widget->xAxis->setTickStep(1);

    ui->widget_2->xAxis->setRange(-10, 10.1);
    ui->widget_2->xAxis->setAutoTickStep(false);
    ui->widget_2->xAxis->setTickStep(2.5);

    ui->widget->yAxis->setRange(0, 1.1);
    ui->widget->yAxis->setAutoTickStep(false);
    ui->widget->yAxis->setTickStep(0.25);

    ui->widget_2->yAxis->setRange(-1.1 , 1.2);
    ui->widget_2->yAxis->setAutoTickStep(false);
    ui->widget_2->yAxis->setTickStep(0.5);


    QPen GridPen(Qt::red, 1);
    GridPen.setStyle(Qt::DotLine);
    GridPen.setColor(QColor(127, 127, 127, 255));
    ui->widget->xAxis->grid()->setPen(GridPen);
    ui->widget->yAxis->grid()->setPen(GridPen);
    ui->widget_2->xAxis->grid()->setPen(GridPen);
    ui->widget_2->yAxis->grid()->setPen(GridPen);

    QFont graphFont = QFont("Sans Serif", 15);

    ui->widget->xAxis->setLabel("Частота, Гц");
    ui->widget_2->xAxis->setLabel("Время, c");

    //ui->widget->yAxis->setLabel("Амплитуда");
    //ui->widget_2->yAxis->setLabel("Отн. Амплитуда");

    ui->widget->xAxis->setLabelFont(graphFont);
    ui->widget_2->xAxis->setLabelFont(graphFont);

    ui->widget->yAxis->setLabelFont(graphFont);
    ui->widget_2->yAxis->setLabelFont(graphFont);

    ui->widget->xAxis->setTickLabelFont(graphFont);
    ui->widget_2->xAxis->setTickLabelFont(graphFont);

    ui->widget->yAxis->setTickLabelFont(graphFont);
    ui->widget_2->yAxis->setTickLabelFont(graphFont);

    ui->widget->legend->setFont(graphFont);

    ui->widget->xAxis->setSubTickLength(5);
    ui->widget->xAxis->setTickLength(10);
    ui->widget->xAxis->setSubTickCount(4);

    ui->widget_2->xAxis->setSubTickLength(5);
    ui->widget_2->xAxis->setTickLength(10);
    ui->widget_2->xAxis->setSubTickCount(4);


    ui->stackedWidget->setCurrentIndex(0);

    ui->stackedWidget_3->setCurrentIndex(0);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_2_clicked()
{
    mode = new Mode(DISCRETE_MODE, ui);
}

void MainWindow::on_pushButton_3_clicked()
{
    mode = new Mode(CONTINUOUS_MODE, ui);
}

void MainWindow::on_comboBox_activated(int index)
{
    mode->changeSubmode(index);
}

void MainWindow::on_pushButton_clicked()
{
    delete mode;
    mode = 0;
}

void MainWindow::on_pushButton_4_clicked()
{
    QApplication::quit();
}

// Непрерывные

void MainWindow::on_doubleSpinBox_valueChanged(double arg1)
{
    if (mode != 0 && mode->smode != 0)
        mode->smode->changeParametr(0, arg1);
}

void MainWindow::on_doubleSpinBox_2_valueChanged(double arg1)
{
    if (mode != 0 && mode->smode != 0)
        mode->smode->changeParametr(1, arg1);
}

// ***********************************************************

// Эквидистантные


void MainWindow::on_spinBox_valueChanged(int arg1)
{
    if (mode != 0 && mode->smode != 0)
        mode->smode->changeParametr(0, arg1);
}

void MainWindow::on_doubleSpinBox_9_valueChanged(double arg1)
{
    if (mode != 0 && mode->smode != 0)
        mode->smode->changeParametr(1, arg1);
}

void MainWindow::on_doubleSpinBox_10_valueChanged(double arg1)
{
    if (mode != 0 && mode->smode != 0)
        mode->smode->changeParametr(2, arg1);
}

// ***********************************************************


// Неэквидистантные

void MainWindow::on_spinBox_2_valueChanged(int arg1)
{
    if (mode != 0 && mode->smode != 0)
        mode->smode->changeParametr(0, arg1);
}

void MainWindow::on_spinBox_3_valueChanged(int arg1)
{
    if (mode != 0 && mode->smode != 0)
        mode->smode->changeParametr(1, arg1);
}

void MainWindow::on_doubleSpinBox_3_valueChanged(double arg1)
{
    if (mode != 0 && mode->smode != 0)
        mode->smode->changeParametr(2, arg1);
}

void MainWindow::on_doubleSpinBox_4_valueChanged(double arg1)
{
    if (mode != 0 && mode->smode != 0)
        mode->smode->changeParametr(3, arg1);
}

void MainWindow::on_pushButton_5_clicked()
{
    if (mode != 0 && mode->smode != 0)
        mode->smode->changeParametr(0, 0);
}

void MainWindow::on_pushButton_6_clicked()
{
    ui->stackedWidget_3->setCurrentIndex(1);
}

void MainWindow::on_pushButton_7_clicked()
{
    ui->stackedWidget_3->setCurrentIndex(0);
}
