#include "nonequdistant.h"

Nonequdistant::Nonequdistant(SUBMODE_TYPE newtype, QSpinBox *newnumber, QSpinBox *newcurrent, QDoubleSpinBox *newfreq, QDoubleSpinBox *newampl, QCustomPlot *newspectrum, QCustomPlot *newsignal, QLabel *newsubheader) :
    QObject(0) , Submode(newtype, newspectrum, newsignal, newsubheader),
    number(newnumber), current(newcurrent), freq(newfreq), ampl(newampl), moving(false)
{
    changeParametr(0, 1);
    needToReplot = true;
    spectrum->legend->setVisible(true);
    replot();

    //mouse interface
    connect(spectrum, SIGNAL(mousePress(QMouseEvent*)), this, SLOT(pressSpectrum(QMouseEvent*)));
    connect(spectrum, SIGNAL(mouseMove(QMouseEvent*)), this, SLOT(moveSpectrum(QMouseEvent*)));
    connect(spectrum, SIGNAL(mouseRelease(QMouseEvent*)), this, SLOT(releaseSpectrum()));
}

void Nonequdistant::setView(int i)
{
    spectrum->graph()->setLineStyle(QCPGraph::lsImpulse);
    spectrum->graph()->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle, 10));
    spectrum->graph()->setName(QString::number(i + 1));
    switch (i) {
    case 0:
        specPen.setColor(Qt::black);
        break;
    case 1:
        specPen.setColor(Qt::blue);
        break;
    case 2:
        specPen.setColor(Qt::red);
    default:
        break;
    }
    spectrum->graph()->setPen(specPen);
}

void Nonequdistant::replot()
{
    if (needToReplot == false)
        return;

    spectrum->clearGraphs();
    for (int i = 0; i < specX.size(); i++) {
        spectrum->addGraph();
        spectrum->graph()->setData({specX[i]}, {specY[i]});
        setView(i);
    }
    spectrum->replot();

    sigX.resize(100 * 20 + 1); sigY.resize(100 * 20 + 1);
    double dt = 20.0 / (sigX.size() - 1);
    for (int i = 0; i < sigX.size(); i++) {
        sigY[i] = 0.0;
        sigX[i] = i * dt - 10;
        for(int j = 0; j < specX.size(); j++) {
            sigY[i] += specY[j] * cos(2 * M_PI * specX[j] * sigX[i]);
        }
        sigY[i] /= specX.size(); //normed result
    }

    signal->clearGraphs();
    signal->addGraph();
    signal->graph()->setData(sigX, sigY);
    signal->graph()->setPen(sigPen);
    signal->replot();
}

void Nonequdistant::changeParametr(size_t index, double value)
{
    switch (index)
    {
        case 0: // Количество гармоник
            current->setMaximum(int(value));
            specX.resize(int(value));
            specY.resize(int(value));
            switch (int(value))
            {
                case 1:
                    subheader->setText(QString("Гармонический сигнал"));
                    specX[0] = 0.2;
                    specY[0] = 1;
                break;
                case 2:
                    subheader->setText(QString("Картина биений"));
                    specX[0] = 1;
                    specY[0] = 1;

                    specX[1] = 1.1;
                    specY[1] = 1;
                break;
                case 3:
                    subheader->setText(QString("Амплитудная модуляция"));
                    specX[0] = 2.9;
                    specY[0] = 0.5;

                    specX[1] = 3;
                    specY[1] = 1;

                    specX[2] = 3.1;
                    specY[2] = 0.5;
                break;
            }
        number->setValue(int(value));
        changeParametr(1, 1);
        break;

        case 1:
            current->setValue(int(value));
            freq->setValue(specX[int(value) - 1]);
            ampl->setValue(specY[int(value) - 1]);
        break;
        case 2:
            if (value < 0) {
                value = 0;
            }
            if (value > 5) {
                value = 5;
            }
            specX[current->value() - 1] = value;
            freq->setValue(value);
        break;
        case 3:
            if (value < 0) {
                value = 0;
            }
            if (value > 1) {
                value = 1;
            }
            specY[current->value() - 1] = value;
            ampl->setValue(value);
        break;
        default:
        break;
    }
    replot();
}

void Nonequdistant::pressSpectrum(QMouseEvent *event)
{
    double x, y;
    x = spectrum->xAxis->pixelToCoord(event->pos().x());
    y = spectrum->yAxis->pixelToCoord(event->pos().y());

    double eps = 0.05;
    for (int i = 0; i < specX.size(); i++) {
        double r = sqrt((specX[i] - x) * (specX[i] - x) + (specY[i] - y) * (specY[i] - y));
        if (r < eps) {
            changeParametr(1, i + 1);
            moving = true;
        }
    }
}

void Nonequdistant::moveSpectrum(QMouseEvent *event)
{
    if (moving == true) {
        double x, y;
        x = spectrum->xAxis->pixelToCoord(event->pos().x());
        y = spectrum->yAxis->pixelToCoord(event->pos().y());
        x = round(x * 10) / 10;
        y = round(y * 10) / 10;

        changeParametr(2, x);
        changeParametr(3, y);
    }
}

void Nonequdistant::releaseSpectrum()
{
    moving = false;
}

Nonequdistant::~Nonequdistant()
{
    spectrum->legend->setVisible(false);

    //disable mouse interface
    disconnect(spectrum, SIGNAL(mousePress(QMouseEvent*)), this, SLOT(pressSpectrum(QMouseEvent*)));
    disconnect(spectrum, SIGNAL(mouseMove(QMouseEvent*)), this, SLOT(moveSpectrum(QMouseEvent*)));
    disconnect(spectrum, SIGNAL(mouseRelease(QMouseEvent*)), this, SLOT(releaseSpectrum()));
}
