#ifndef GAUSS_H
#define GAUSS_H

#include "submode.h"


class Gauss : public Continuous
{
public:
    Gauss(SUBMODE_TYPE newtype, QLineEdit *newcenter, QLineEdit *newwidth);
private:
    double functionValue(double x);
};

#endif // GAUSS_H
