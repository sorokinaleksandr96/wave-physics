#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    QPalette Pal;
    Pal.setColor(QPalette::Background, QColor(0xCC, 0xCC, 0xFF));
    w.setAutoFillBackground(true);
    w.setPalette(Pal);
    w.showFullScreen();

    return a.exec();
}
