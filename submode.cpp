#include "submode.h"

Submode::Submode(SUBMODE_TYPE newtype, QCustomPlot *newspectrum, QCustomPlot *newsignal, QLabel *newsubheader) : type(newtype), needToReplot(false),
    spectrum(newspectrum), signal(newsignal), specPen(Qt::blue, 3), sigPen(Qt::red, 3), subheader(newsubheader)
{

}

SUBMODE_TYPE Submode::getType()
{
    return type;
}

Submode::~Submode()
{

}
