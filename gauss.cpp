#include "gauss.h"

Gauss::Gauss(SUBMODE_TYPE newtype, QLineEdit *newcenter, QLineEdit *newwidth): Continuous(newtype, newcenter, newwidth)
{

}

double Gauss::functionValue(double x)
{
    return exp(-(x - center) * (x - center) / (2 * width * width));
}
