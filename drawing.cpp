#include "drawing.h"
#include "fft.h"

Drawing::Drawing(SUBMODE_TYPE newtype, QCustomPlot *newspectrum, QCustomPlot *newsignal, QPushButton *newclear, QLabel *newsubheader) :
    QObject(0), Submode(newtype, newspectrum, newsignal, newsubheader), clear(newclear),
    is_drawing(false), is_drawed(false), is_splined(false)
{

    subheader->setText("Произвольный спектр");

    replot();
    //mouse Interface
    connect(spectrum, SIGNAL(mousePress(QMouseEvent*)), this, SLOT(startDrawing(QMouseEvent*)));
    connect(spectrum, SIGNAL(mouseMove(QMouseEvent*)), this, SLOT(moveDrawing(QMouseEvent*)));
    connect(spectrum, SIGNAL(mouseRelease(QMouseEvent*)), this, SLOT(endDrawing()));
}

void Drawing::splineProcess()
{
    if (is_splined || specX.size() < 3) {
        return;
    }

    a = specY;
    f = specX;
    size_t N = a.size();
    b.resize(N);
    c.resize(N);
    d.resize(N);

    QVector<double> alpha(N - 1), beta(N - 1);
    double A, B, C, D, cur_h, next_h, z;
    alpha[0] = beta[0] = 0;

    for (size_t i = 1; i < N - 1; i++) {
        cur_h = f[i] - f[i - 1]; next_h = f[i + 1] - f[i];
        A = cur_h; B = next_h;
        C = 2 * (A + B);
        D = 6 * ((a[i + 1] - a[i]) / next_h - (a[i] - a[i - 1]) / cur_h);
        z = A * alpha[i - 1] + C;
        alpha[i] = -B / z;
        beta[i] = (D - A * beta[i - 1]) / z;
    }

    c[0] = c[N - 1] = 0.0;
    for (size_t i = N - 2; i > 0; i--) {
        c[i] = alpha[i] * c[i + 1] + beta[i];
    }

    for (size_t i = N - 1; i > 0; i--) {
        cur_h = f[i] - f[i - 1];
        d[i] = (c[i] - c[i - 1]) / cur_h;
        b[i] = cur_h * (2 * c[i] + c[i - 1]) / 6 + (a[i] - a[i - 1]) / cur_h;
    }

    is_splined = true;
}

double Drawing::getSplineValue(double x)
{
    if (x < f[0] || x > f.back()) {
        return 0.0;
    } else {
        int i = 0, j = f.size() - 1;
        while (i + 1 < j) {
            int k = i + (j - i) / 2;
            if (x <= f[k]) {
                j = k;
            } else {
                i = k;
            }
        }
        double dx = x - f[j];
        return a[j] + (b[j] + (c[j] / 2.0 + d[j] * dx / 6.0) * dx) * dx;
    }
}

void Drawing::replot()
{
    spectrum->clearGraphs();
    spectrum->addGraph();
    if (is_splined) {
        QVector<double> x(501), y(501);
        for (int i = 0; i < 501; i++) {
            x[i] = i * 0.01;
            y[i] = getSplineValue(x[i]);
        }
        spectrum->graph()->setData(x, y);
    } else {
        spectrum->graph()->setData(specX, specY);
        if (specX.size() < 3 && is_drawed) {
            spectrum->graph()->setLineStyle(QCPGraph::lsImpulse);
        }
    }
    spectrum->graph()->setPen(specPen);
    spectrum->replot();

    //signal
    signal->clearGraphs();
    if (is_splined) {
        QVector<double> x(4001), y(4001);
        for (int i = 0; i < 4001; i++) {
            x[i] = i * 0.01;
            y[i] = getSplineValue(x[i]);
        }

        fft FFT(y);
        FFT.process();
        signal->addGraph();
        sigX = FFT.getArg(0.025);
        sigY = FFT.getRealValue(0.01);
        double max = 0;
        for (int i = 0; i < sigY.size(); i++) {
            if (fabs(sigY[i]) > max) {
                max = fabs(sigY[i]);
            }
        }
        for (int i = 0; i < sigY.size(); i++) {
            sigY[i] /= max;
        }
        signal->graph()->setData(sigX, sigY);
        signal->graph()->setPen(sigPen);
    }
    signal->replot();
}

void Drawing::changeParametr(size_t index, double value)
{
    switch (index) {
    case 0:
        if (value == 0) {
            specX.resize(0);
            specY.resize(0);
            is_drawed = false;
            is_splined = false;
        }
    break;
    default:
    break;
    }

    replot();
}

void Drawing::startDrawing(QMouseEvent *event)
{
    double x, y;
    x = spectrum->xAxis->pixelToCoord(event->pos().x());
    y = spectrum->yAxis->pixelToCoord(event->pos().y());
    if (x < 0) {
        x = 0;
    }
    if (x > 5) {
        x = 5;
    }
    if (y > 1) {
        y = 1;
    }
    if (y < 0) {
        y = 0;
    }

    if(is_drawed == false) {
        is_drawing = true;
        specX.push_back(x);
        specY.push_back(y);
        replot();
    }
}

void Drawing::moveDrawing(QMouseEvent *event)
{
    double x, y;
    x = spectrum->xAxis->pixelToCoord(event->pos().x());
    y = spectrum->yAxis->pixelToCoord(event->pos().y());
    if (x < 0) {
        x = 0;
    }
    if (x > 5) {
        x = 5;
    }
    if (y > 1) {
        y = 1;
    }
    if (y < 0) {
        y = 0;
    }

    if(is_drawing) {
        if (x > specX.back()) {
            specX.push_back(x);
            specY.push_back(y);
            replot();
        }
    }
}

void Drawing::endDrawing()
{
    is_drawed = true;
    is_drawing = false;

    splineProcess();
    replot();
}

Drawing::~Drawing()
{
    //disconnect mouse Interface
    disconnect(spectrum, SIGNAL(mousePress(QMouseEvent*)), this, SLOT(startDrawing(QMouseEvent*)));
    disconnect(spectrum, SIGNAL(mouseMove(QMouseEvent*)), this, SLOT(moveDrawing(QMouseEvent*)));
    disconnect(spectrum, SIGNAL(mouseRelease(QMouseEvent*)), this, SLOT(endDrawing()));
}
