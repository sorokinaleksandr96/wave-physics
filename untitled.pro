#-------------------------------------------------
#
# Project created by QtCreator 2016-10-31T23:10:53
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = untitled
TEMPLATE = app



SOURCES += main.cpp\
        mainwindow.cpp \
    submode.cpp \
    continuous.cpp \
    mode.cpp \
    nonequdistant.cpp \
    equdistant.cpp \
    qcustomplot.cpp \
    fft.cpp \
    drawing.cpp

HEADERS  += mainwindow.h \
    submode.h \
    continuous.h \
    mode.h \
    nonequdistant.h \
    equdistant.h \
    qcustomplot.h \
    fft.h \
    drawing.h

FORMS    += mainwindow.ui

RESOURCES += \
    myres.qrc

DISTFILES +=
