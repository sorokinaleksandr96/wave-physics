#include "fft.h"
#include "algorithm"


fft::fft(const QVector<double> &value) : res(value.size())
{
    for (int i = 0; i < res.size(); i++) {
        res[i] = value[i];
    }
}



int fft::getNext2Pow(int N)
{
    int res = 1;
    while (res < N) {
        res *= 2;
    }
    return res; // for more accuracy, but actually doesnt work well enough
}

void fft::fill2Pow()
{
    int size = res.size();
    int N = getNext2Pow(size);
    for (int i = 0; i < N - size; i++) {
        if (i % 2) {
            res.push_back(0);
        } else {
            res.insert(res.begin(), 0);
        }
    }
}


void fft::fftAlgorithm()
{
    int n = res.size();

    for (int i = 1, j = 0; i < n; i++) {
        int bit = n >> 1;
        for (; j >= bit; bit >>= 1)
            j -= bit;
        j += bit;
        if (i < j) {
            std::swap(res[i], res[j]);
        }
    }

    for (int len = 2; len <= n; len <<= 1) {
        for (int i=0; i<n; i+=len) {
            for (int j=0; j<len/2; ++j) {
                double ang = 2 * j * M_PI / len;
                base w(cos(ang), sin(ang));
                base u = res[i+j],  v = res[i+j+len/2] * w;
                res[i+j] = u + v;
                res[i+j+len/2] = u - v;
            }
        }
    }
}

void fft::process()
{
    fill2Pow();
    fftAlgorithm();
}

QVector<double> fft::getRealValue(double dnu)
{
    QVector<double> value(res.size() + 1);
    for (int i = 0; i <= value.size() / 2; i++) {
        value[i + value.size() / 2] = std::real(res[i]) * dnu * sqrt(2 * M_PI);
    }
    for (int i = 0; i < value.size() / 2; i++) {
        value[i] = std::real(res[i + value.size() / 2]) * dnu * sqrt(2 * M_PI);
    }
    return value;
}

QVector<double> fft::getArg(double dt)
{
    QVector<double> arg(res.size() + 1);
    for (int i = 0; i < arg.size(); i++) {
        arg[i] = (i - arg.size() / 2) * dt;
    }
    return arg;
}
