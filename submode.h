#ifndef SUBMODE_H
#define SUBMODE_H

#include <QVector>
#include <QRadioButton>
#include <QLabel>
#include <QDoubleSpinBox>
#include <QSpinBox>
#include <QPen>
#include "qcustomplot.h"


enum SUBMODE_TYPE
{
    EQUDISTANT_SUBMODE,
    NONEQUDISTANT_SUBMODE,
    GAUSSIAN_SUBMODE,
    RECTANGLE_SUBMODE,
    SELFDRAWING_SUBMODE,
    LORENZ_SUBMODE
};

class Submode
{
public:
    Submode(SUBMODE_TYPE newtype, QCustomPlot *newspectrum, QCustomPlot *newsignal, QLabel *newsubheader);
    virtual void replot() = 0;
    virtual void changeParametr(size_t index, double value) = 0;
    virtual ~Submode();
    SUBMODE_TYPE getType();

protected:
    SUBMODE_TYPE type;
    bool needToReplot;
    QCustomPlot *spectrum, *signal;
    QVector<double> specX, specY, sigX, sigY;
    QPen specPen, sigPen;
    QLabel *subheader;
};

#endif // SUBMODE_H
